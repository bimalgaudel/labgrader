labgrader
==========
Using python to help grade lab reports.

Cli prompts for topic-wise grading of lab reports based on a rubric.

Installation
============

Install dependencies:
`pip3 install json pyperclip`

Then install `labgrader`:

`pip3 install --user git+ssh://git@github.com/bimalgaudel/labgrader.git#egg=labgrader`, or

`pip3 install --user git+https://github.com/bimalgaudel/labgrader.git#egg=labgrader`

Example
=======
[![asciicast](https://asciinema.org/a/nb0nlaRg4GpHxwxfp5xaOWMX1.svg)](https://asciinema.org/a/nb0nlaRg4GpHxwxfp5xaOWMX1)
