import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="labgrader",
    version="0.0.1",
    author="Bimal Gaudel",
    author_email="bimalgaudel@gmail.com",
    description="Small package to improve workflow of grading lab reports",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/bimalgaudel/labgrader",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
