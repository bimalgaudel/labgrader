import os
try:
    from labgrader import Session
    os.chdir('./tests')
except:
    from session import Session

s = Session('test')
s.add_pupils('Bimal', 'Bimala', 'Bijay')
s.grade_session()
s.comment_session()
