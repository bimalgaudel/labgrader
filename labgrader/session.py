import os
import json
import pyperclip

class Session(object):
    """
    An object to handle a laboratory reports grading session.
    """

    def __init__(self, exp_name, file_rubric="", file_log=""):
         
        self.exp_name    = exp_name
        self.file_rubric = file_rubric if file_rubric else self.exp_name + ".rub"
        self.file_log    = file_log if file_log else self.exp_name + ".json"
         
        self.dict_rubric            = {}
        self.dict_log               = {}
        self.pupils_ungraded        = []
        self.pupils_graded          = []
     
        # order matters as load_logfile() reads dict_rubric.keys()
        self.load_rubricfile()
        self.load_logfile()

    def load_rubricfile(self):
        assert os.path.isfile(self.file_rubric), "Rubric file not found!"
        try:
            self.dict_rubric = self._parse_rubric(self.file_rubric)
        except:
            print("Error loading the rubric file!")

    def load_logfile(self):
        if self.file_log and os.path.isfile(self.file_log):
            with open(self.file_log, 'r') as f:
                try:
                    self.dict_log = json.load(f)
                    log_keys      = self.dict_log.keys()
                    rubric_keys   = self.dict_rubric.keys()
                    for p in log_keys:
                        if set(rubric_keys) != set(self.dict_log[p].keys()):
                            self.pupils_ungraded.append(p) # partially graded students added
                        else:
                            self.pupils_graded.append(p)
                except:
                    print("Error loading the log file!")
                    raise
        else:
            pass

    def _parse_rubric(self, rub_file):
        "Parse a rubric file and return a dictionary object"
         
        def is_topic(string):
            return string[0] == '[' and string[-1] == ']'

        def format_topic(rub, index):
            t = rub[index]
            rub[index] = '}, '+'"'+t.strip('[]')+'"'+': {'

        def format_entry(rub, index):
            fmted = rub[index]
            fmted = fmted.replace(':', '":').replace('[', '["').replace(', ', '", "').replace(']', '"]')
            fmted = '"'+fmted+', '
            rub[index] = fmted
         
        with open(rub_file, 'r') as f:
            rub = f.readlines()
         
        rub = [l.strip() for l in rub if l[0] != '#' and l != '\n']
        for i in range(len(rub)):
            if is_topic(rub[i]):
                format_topic(rub, i)
            else:
                format_entry(rub, i)
        # keep formatting
        rub[0] = rub[0].replace('}, ', '{')
        rub.append('}}')
        rub_string = ''.join(rub)
        rub_string = rub_string.replace(', }', '}')
         
        return json.loads(rub_string)

    def write_log(self):
        with open(self.file_log, 'w') as f:
            json.dump(self.dict_log, f)

    def grade_topic(self, topic):
        "Print the prompts and get scores"
         
        prompts      = topic.get("prompts", [])
        rub_comments = topic.get("comments", [])
        full_scores  = topic.get("full_scores", [])
        
        def get_num(g,ulim,llim=0):
            "Get a valid score"
            if not g:
                return ulim
            elif g == ' ':
                return llim
            try:
                g = int(g)
                if (g >= llim) and (g <= ulim):
                    return(g)
                else:
                    return get_num(input("    The range could be %d to %d:    " % (llim, ulim)), ulim, llim)
            except:
                print("    Please enter a valid number:    ")
                return get_num(input(), ulim, llim)
         
        got_scores = []
        comments   = []
         
        for i in range(len(prompts)):
            low_score_lim = 0
            up_score_lim  = int(full_scores[i])
            raw_grade = input(prompts[i] + '? (%d-%d)    ' % (low_score_lim, up_score_lim))
            raw_grade = raw_grade.split('+')
             
            s = get_num(raw_grade[0], up_score_lim) 
            got_scores.append(s)
             
            if int(s) == low_score_lim and len(raw_grade) == 1:
                comments.append(rub_comments[i])
             
            comments += [i.strip() for i in raw_grade[1:]]
         
        return {"scores": got_scores, "comments": comments}

    def grade_student(self, name):
        "Prompt and get grades for all topics for a student"
         
        grades = {}
         
        for k in self.dict_rubric.keys():
            print("   ## Grading *%s*" % (k))
            grades[k] = self.grade_topic(self.dict_rubric[k])
         
        return grades

    def grade_session(self):
        'Do grading for all students in current Session'''
         
        to_grade = self.pupils_ungraded[:]
         
        for p in to_grade:
            grades = self.dict_log.get(p, {})  # check for existing grades
            topics = [t for t in self.dict_rubric.keys() if t not in grades.keys()]
             
            if not topics:
                print("  [x] Everything graded for %s" % p)
                return
             
            self.clear_screen()
            header = "Grading work of # %s #" % p
            header += '\n'+'-'*len(header)
            print(header)
            self.dict_log[p] = self.grade_student(p)
            self.pupils_graded.append(p)
            self.pupils_ungraded.remove(p)
            self.write_log()

    def add_pupils(self, *args):
        "Proper way to add students (names/ids) to a grading session"
        for arg in args:
            if arg in self.pupils_graded:
                print("  %s's work already graded!" % arg)
            elif arg in self.pupils_ungraded:
                print("  %s's already in the list!" % arg)
            else:
                print("  %s's been added to the to-grade list [ok]" % arg)
                self.pupils_ungraded.append(arg)

    def comment_student(self, name):
        "Get the scores and comments of a student"
         
        assert name in self.pupils_graded, "Grades for %s not available!" % name
         
        comment = ["%s's grades on %s lab" % (name, self.exp_name)]
        comment.append('-'*len(comment[0]))
        # name's scores and comments
        score_dict = self.dict_log[name]
        topics = score_dict.keys()
        running_score = 0
        running_fullscore = 0
        for t in topics:
            obtained = sum(int(x) for x in score_dict[t]["scores"])
            full_score = sum(int(x) for x in self.dict_rubric[t]["full_scores"])
            running_score += obtained
            running_fullscore += full_score
            comment.append(f"{t}: {obtained}/{full_score}")
            for c in score_dict[t]["comments"]:
                comment.append(f"    - {c}")
            #
        comment.insert(2, f"Total: {running_score}/{running_fullscore}")
        return "\n".join(comment)

    def comment_student_v2(self, name, full_score=80.0):
        "Prints comment with reduced scores"
         
        assert name in self.pupils_graded, "Grades for %s not available!" % name
         
        comment = ["%s's grades on %s lab" % (name, self.exp_name)]
        comment.append('-'*len(comment[0]))
         
        # name's scores and comments
        score_dict = self.dict_log[name]
        topics = score_dict.keys()
        running_reduced_score = 0

        for t in topics:
            topic_obt_scores = [ int(x) for x in score_dict[t]["scores"] ]
            topic_full_scores = [ int(x) for x in self.dict_rubric[t]["full_scores"] ]
            topic_comments    = score_dict[t]["comments"]
            if topic_comments:
                comment.append(f'{t}\n' + '===')
            iter_comments = iter(topic_comments)
            for s, f in zip(topic_obt_scores, topic_full_scores):
                red = f-s
                if red:
                    running_reduced_score += red
                    try:
                        comment.append(f"    * ( -{red}pts) {next(iter_comments)}")
                    except:
                        continue
            # let's not waste extra feedback
            while(True):
                try:
                    extra_comment = next(iter_comments)
                    comment.append(f"    *          {extra_comment}")
                except:
                    break

        final_obtained = full_score - running_reduced_score
        if final_obtained < 0:
            final_obtained = 0.0
        comment.insert(2, f"Total: {final_obtained}/{full_score}")
        return "\n".join(comment)

    def comment_session(self, v2=False, full_score=80.0):
        for pupil in self.dict_log.keys():
            if v2:
                pyperclip.copy(self.comment_student_v2(pupil, full_score))
            else:
                pyperclip.copy(self.comment_student(pupil))
            print(f"{pupil}'s comments and scores copied to the clipboard [ok]")
            input()
     
    def clear_screen(self):
        """
        Clears the terminal screen.
         
        https://stackoverflow.com/questions/18937058/clear-screen-in-shell/31871439
        """
        from platform import system as system_name # Returns the system/OS name
        from os import system as system_call       # Execute a shell command
         
        # Clear command as function of OS
        command = "-cls" if system_name().lower()=="windows" else "clear"
        # Action
        system_call(command)

